# enames

![](https://img.shields.io/badge/written%20in-PHP-blue)

A web service for generating novelty short email domains.

enames helps you find a short email domain for your full name. It performs a DNS lookup on the target address to guess whether it's available for registration.

Live demo currently unavailable.


## Download

- [⬇️ enames-1.0.rar](dist-archive/enames-1.0.rar) *(6.71 KiB)*
